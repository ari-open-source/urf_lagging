// Type definitions for urf_lagging v1.1.0
// Project: urf_lagging
// Definitions by: Heath Kuntz

interface urf {
  mnth: number;
  urf: number;
}

/*~ If this module has methods, declare them as functions like so.
 */
export function lag(site_values: number[], urfs: urf[]): number[];
