# URF Lagging
This is a package that runs a simple convolution process that we use to lag depletions and accretions based upon a URF value with a array of well pumping and add that together to recieve an out put of values.

## Inputs
- site_values: an array of values at the site of either pumping, cu, or recharge amounts that will be distributed by the URF function
- urfs: an array of urfs that may be used to distribute the pumping. Can be a single urf value or multiple values that include reaches.

## Return
The function returns a single array of values that are the combination of all the site_values with the urfs.