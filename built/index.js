"use strict";
// function that is the convolution to the lagged values
Object.defineProperty(exports, "__esModule", { value: true });
const lag = (site_values, urfs) => {
    // send the site_values to the lagging
    let result_lag = [];
    let single_result;
    site_values.forEach((vals, i) => {
        if (result_lag.length > 0) {
            single_result = timestep_lag(vals, urfs);
            // shift the result down every time to add to the results
            for (let x = 0; x < i; x++) {
                single_result.unshift(0);
            }
            // there is a result already started
            result_lag = sumArray(single_result, result_lag);
        }
        else {
            // no existing result, just set it equal
            result_lag = timestep_lag(vals, urfs);
        }
    });
    // return a single array of values that combine all the lagged values
    // console.log(result_lag);
    return result_lag;
};
const timestep_lag = (site_val, urf) => {
    // console.log(urf);
    return urf.map(u => u.urf * site_val);
};
const sumArray = (a, b) => {
    var c = [];
    for (var i = 0; i < Math.max(a.length, b.length); i++) {
        c.push((a[i] || 0) + (b[i] || 0));
    }
    return c;
};
exports.default = lag;
